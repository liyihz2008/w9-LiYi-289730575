# -*- coding: utf-8 -*-
import os
import time
import cv2
import numpy as npy
from sklearn import decomposition

train_dir = "./train/"
dir_names = os.listdir(train_dir)

for dir_name in dir_names:
    print(">>>>>>>>>>>>>>>>>正在查询目录：" + dir_name)
    img_name_array = os.listdir(train_dir + str(dir_name))
    for img_name in img_name_array:
        img_path = train_dir + str(dir_name) + "/" + str(img_name)
        # 图片加载
        image = cv2.imread(img_path)
        # 重新设置大小
        image = cv2.resize(image, (512, 512))
        # 图片灰度处理
        image_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        
        #特征点检测器，可以迭代多个检测器进行特征点检测
        #SIFT特征点检测器（尺度不变，局部特征的描述）- ( 噪点较多)
        sift = cv2.xfeatures2d.SIFT_create()
        key_points = sift.detect(image_gray, None)
        cv2.drawKeypoints(image, key_points, image, (0, 255, 0), flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT)
        cv2.imshow("SIFT", image)
        
        #特征点检测器Star( 噪点较少)
        star = cv2.xfeatures2d.StarDetector_create()
        key_points = star.detect(image_gray, None)
        image = cv2.imread(img_path)
        cv2.drawKeypoints(image, key_points, image, (0, 255, 0), flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT)
        cv2.imshow("Star", image)

        #特征点检测器SURF( 噪点最多 )
        SURF = cv2.xfeatures2d.SURF_create()
        key_points = SURF.detect(image_gray, None)
        image = cv2.imread(img_path)
        cv2.drawKeypoints(image, key_points, image, (0, 255, 0), flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT)
        cv2.imshow("SURF", image)
        
        #迭代特征点检测Star - SIFT( 噪点较少)
        star = cv2.xfeatures2d.StarDetector_create()
        key_points = star.detect(image_gray, None)
        sift = cv2.xfeatures2d.SIFT_create()
        key_points, features = sift.compute(image_gray, key_points)
        image = cv2.imread(img_path)
        cv2.drawKeypoints(image, key_points, image, (0, 255, 0), flags=cv2.DRAW_MATCHES_FLAGS_DEFAULT)
        #cv2.imshow("Star - SIFT", image)

        #通过对三种检测器的比较，Star检测器的效果相对比较好，噪点较少，SURF检测器的噪点最多
        # SIFT 可以通过参数控制取到的关键点的数量，但是其他的检测器还没有发现有什么方法控制数量
        break
    break
        

   



