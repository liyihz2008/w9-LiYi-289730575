# -*- coding: utf-8 -*-
import os
import cv2
import numpy as npy
from sklearn import decomposition

train_dir = "./train/"
animal_type_lables = dir_names = os.listdir(train_dir)


#特征提取
def get_features( file_path ):
    this_img = cv2.imread(file_path)
    this_img = cv2.resize(this_img, (512, 512))
    this_img_gray = cv2.cvtColor(this_img, cv2.COLOR_BGR2GRAY)
    sift = cv2.xfeatures2d.SIFT_create(nfeatures =20)
    key_points, features = sift.detectAndCompute(this_img_gray, None)
    return features;

x_features = []
y_labels = []
index = 0
for dir_name in dir_names:
    index += 1
    print(">>>>>>>>>>>>>>>>>正在查询目录：" + dir_name)
    img_name_array = os.listdir(train_dir + str(dir_name))
    for img_name in img_name_array:
        img_path = train_dir + str(dir_name) + "/" + str(img_name)
        features = get_features(img_path)
        x_features.append(features)
        y_labels.append(index)

print("每个图像特征点数量：" + str(len(x_features[0])))
print("标签数据集：")
print(y_labels)

   



